#ifndef TEXTMESSAGEWINDOW_H
#define TEXTMESSAGEWINDOW_H

#include <QWidget>
#include <QTextTable>
#include <QLineEdit>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QLabel>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QSqlError>
#include <QSqlQuery>
#include <QDateTime>
#include <ctime>

#include "common-library/cellularradio.h"
class TextMessageWindow : public QWidget
{
    Q_OBJECT
public:
    TextMessageWindow(QString ConvID, CellularRadio * Radio);
    //TextMessageWindow(QString ContactID);

public slots:
    void ReturnPressed();

signals:
    void MessageSent();

private:
    void AddMessage(QString Source, QString Message);
    void BasicInit();
    //QTextTable m_List;
    QLabel * m_Recipient;
    QLineEdit * m_NewMessage;
    QTextEdit * m_MessageList;
    QVBoxLayout * m_Layout;
    QString m_ConvID;
    CellularRadio * m_Radio;
};

#endif // TEXTMESSAGEWINDOW_H
