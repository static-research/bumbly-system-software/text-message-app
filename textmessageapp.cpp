#include "textmessageapp.h"

TextMessageApp::TextMessageApp() :
    m_MessageScrollArea(nullptr),
    m_MessageLayout(nullptr),
    m_MessageThreads(nullptr),
    m_Radio(nullptr),
    m_GlobalLayout(nullptr)
{
    this->showFullScreen();
    QSqlDatabase DataBase = QSqlDatabase::addDatabase("QMYSQL");
    /*TODO: Switch to user authentication*/
    DataBase.setHostName("127.0.0.1");
    DataBase.setUserName("root");
    DataBase.setPassword("bumblee");
    DataBase.setDatabaseName("Bumbly");
    if(!DataBase.open())
    {
        QMessageBox::information(this, "No valid Connection", "We didn't connect to the database properly, oops!");
    }
    m_Radio = new CellularRadio("/dev/ttyUSB2", "115200");
    m_NetworkInfo = new NetworkInfo;
    m_GlobalLayout = new QVBoxLayout;
    m_GlobalLayout->addWidget(m_NetworkInfo);
    m_NetworkInfo->SetCarrierName(m_Radio->GetCarrier());
    m_NetworkInfo->SetPhoneNumber(m_Radio->GetNumber());
    m_CheckForMessages = new QToolButton;
    m_CheckForMessages->setText("Check for New Messages");
    m_CheckForMessages->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    m_GlobalLayout->addWidget(m_CheckForMessages);
    m_CheckForMessages->setDown(true);
    connect(m_CheckForMessages, &QToolButton::clicked, this, &TextMessageApp::CheckForNewMessages);
    this->setLayout(m_GlobalLayout);
    UpdateMessageList();
    connect(m_Radio, &CellularRadio::TextMessageReceived, this, &TextMessageApp::ReceiveTextMessage);
    m_Timer = new QTimer;
    m_Timer->setInterval(250);
    connect(m_Timer, &QTimer::timeout, this, &TextMessageApp::CheckForNewMessages);
    m_Timer->start();
    //m_Radio->start();
}

TextMessageApp::~TextMessageApp()
{
    delete[] m_MessageThreads;
    delete m_MessageLayout;
}

void TextMessageApp::ReceiveTextMessage(TextMessage msg)
{
    QSqlQuery AddressBookQuery;
    QSqlQuery MessageQuery;
    QSqlQuery ConvQuery;
    //Find Contact by phone number
    AddressBookQuery.prepare("SELECT * FROM Contacts WHERE phonenumber=" + msg.GetSource());
    AddressBookQuery.exec();
    AddressBookQuery.next();
    if (AddressBookQuery.isValid())
    {
        QMessageBox::information(this, "Received Message From", AddressBookQuery.value("name").toString());
        ConvQuery.prepare("SELECT * FROM conversation where Destination=" + AddressBookQuery.value("c_id").toString());
        ConvQuery.exec();
        ConvQuery.next();
        MessageQuery.prepare("INSERT INTO conversation_reply (reply, dest_id, time, conv_id)"
                             "VALUES(:reply, :dest_id, :time, :conv_id)");
        MessageQuery.bindValue(":reply", msg.GetMessage());
        MessageQuery.bindValue(":dest_id", AddressBookQuery.value("c_id").toString());
        MessageQuery.bindValue(":time", QDateTime::currentSecsSinceEpoch()); //FIXME: Should get real time from cell module
        MessageQuery.bindValue(":conv_id", ConvQuery.value("conv_id").toString());
        if(!MessageQuery.exec())
        {
            QMessageBox::information(this, "Error Adding Message to Database", MessageQuery.lastError().text());
        }
    }
    else
    {
        QMessageBox::information(this, "Received Message From", msg.GetSource());
        QMessageBox::information(this, "Message", msg.GetMessage());
    }
    UpdateMessageList();
}

void TextMessageApp::StartAddressBook()
{
    AddressBook * myAddr = new AddressBook;
    myAddr->setWindowModality(Qt::ApplicationModal);
    myAddr->show();
    connect(myAddr, &AddressBook::ContactAccepted, this, &TextMessageApp::StartNewMessageThread);
}

void TextMessageApp::StartExistingMessageThread()
{
    QToolButton * SendingButton = static_cast<QToolButton *>(sender());
    TextMessageWindow * MessageWindow = new TextMessageWindow(SendingButton->property("cr_id").toString(), m_Radio);
    connect(MessageWindow, &TextMessageWindow::MessageSent, this, &TextMessageApp::UpdateMessageList);
    MessageWindow->show();
    //QMessageBox::information(this, "TextMessageApp", SendingButton->property("cr_id").toString());
}

void TextMessageApp::UpdateMessageList()
{
    delete[] m_MessageThreads;
    m_MessageThreads = nullptr;
    delete m_MessageLayout;
    m_MessageLayout = nullptr;
    delete m_MessageScrollArea;
    m_MessageScrollArea = nullptr;
    int NumberOfMessages = -1;
    m_MessageLayout = new QVBoxLayout;
    QSqlQuery MessageQuery;
    QSqlQuery AddressBookQuery;
    QSqlQuery MostRecentMessageQuery;

    MessageQuery.prepare("SELECT conv_id, Destination FROM conversation;");
    MessageQuery.exec();
    NumberOfMessages = MessageQuery.size();
    MessageQuery.next();
    if(NumberOfMessages == -1)
    {
        m_MessageThreads = new QToolButton[1];
    }
    else
    {
        m_MessageThreads = new QToolButton[NumberOfMessages + 1];
        for(int idx = 1; idx < NumberOfMessages + 1; ++idx)
        {
            AddressBookQuery.prepare("SELECT name FROM Contacts WHERE c_id=" + MessageQuery.value("Destination").toString());
            AddressBookQuery.exec();
            AddressBookQuery.next();
            MostRecentMessageQuery.prepare("SELECT reply FROM conversation_reply WHERE conv_id=" + MessageQuery.value("conv_id").toString());
            MostRecentMessageQuery.exec();
            MostRecentMessageQuery.last();
            m_MessageThreads[idx].setText(AddressBookQuery.value("name").toString() + " : " + MostRecentMessageQuery.value("reply").toString());
            m_MessageThreads[idx].setProperty("cr_id", MessageQuery.value("conv_id").toString());
            connect(&m_MessageThreads[idx], &QToolButton::clicked, this, &TextMessageApp::StartExistingMessageThread);
            m_MessageThreads[idx].setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
            m_MessageLayout->addWidget(&m_MessageThreads[idx]);
            MessageQuery.next();
        }
    }
    m_MessageThreads[0].setText("New Message");
    m_MessageThreads[0].setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    m_MessageLayout->insertWidget(0, &m_MessageThreads[0]);
    connect(&m_MessageThreads[0], &QToolButton::clicked, this, &TextMessageApp::StartAddressBook);
    m_GlobalLayout->addLayout(m_MessageLayout);
    this->update();
}

void TextMessageApp::CheckForNewMessages()
{
    m_Radio->CheckForMessages();
}

void TextMessageApp::StartNewMessageThread(QString AcceptedContact)
{
    //QMessageBox::information(this, "TextMessageApp", AcceptedContact);
    QSqlQuery MessageQuery;
    MessageQuery.prepare("INSERT INTO conversation (Destination, time)"
                         "VALUES(:Destination, :time)");
    MessageQuery.bindValue(":Destination", AcceptedContact);
    MessageQuery.bindValue(":time", QDateTime::currentSecsSinceEpoch());
    if(!MessageQuery.exec())
    {
        QMessageBox::information(this, "Error Adding Message to Database", MessageQuery.lastError().text());
    }
    MessageQuery.prepare("SELECT conv_id FROM conversation WHERE Destination=" + AcceptedContact);
    MessageQuery.exec();
    MessageQuery.next();
    TextMessageWindow * MessageWindow = new TextMessageWindow(MessageQuery.value("conv_id").toString(), m_Radio);
    connect(MessageWindow, &TextMessageWindow::MessageSent, this, &TextMessageApp::UpdateMessageList);
    MessageWindow->show();
}
