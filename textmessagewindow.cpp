#include "textmessagewindow.h"

TextMessageWindow::TextMessageWindow(QString ConvID, CellularRadio * Radio):
    m_Radio(Radio)
{
    this->showFullScreen();
    BasicInit();
    m_ConvID = ConvID;
    QSqlQuery MessageQuery;
    QSqlQuery AddressBookQuery;
    MessageQuery.prepare("SELECT Destination FROM conversation WHERE conv_id=" + ConvID);
    MessageQuery.exec();
    MessageQuery.next();
    AddressBookQuery.prepare("SELECT name FROM Contacts WHERE c_id=" + MessageQuery.value("Destination").toString());
    AddressBookQuery.exec();
    AddressBookQuery.next();
    m_Recipient->setText(AddressBookQuery.value("name").toString());
    MessageQuery.prepare("SELECT * FROM conversation_reply WHERE conv_id=" + ConvID + ";");
    MessageQuery.exec();
    MessageQuery.next();
    for (int idx = 0; idx < MessageQuery.size();MessageQuery.next(), ++idx)
    {
        AddressBookQuery.prepare("SELECT name FROM Contacts WHERE c_id=" + MessageQuery.value("dest_id").toString());
        AddressBookQuery.exec();
        AddressBookQuery.next();
        AddMessage(AddressBookQuery.value("name").toString(), MessageQuery.value("reply").toString());
    }
}

void TextMessageWindow::ReturnPressed()
{
    QLineEdit * SendingBox = static_cast<QLineEdit *>(sender());
    QSqlQuery AddressBookQuery;
    QSqlQuery MessageQuery;
    MessageQuery.prepare("INSERT INTO conversation_reply (reply, dest_id, time, conv_id)"
                         "VALUES(:reply, :dest_id, :time, :conv_id)");
    MessageQuery.bindValue(":reply", SendingBox->text());
    MessageQuery.bindValue(":dest_id", 0);
    MessageQuery.bindValue(":time", QDateTime::currentSecsSinceEpoch());
    MessageQuery.bindValue(":conv_id", m_ConvID);
    if(!MessageQuery.exec())
    {
        QMessageBox::information(this, "Error Adding Message to Database", MessageQuery.lastError().text());
    }
    else
    {

        QString Destination;
        QString Text;
        MessageQuery.prepare("SELECT * FROM conversation WHERE conv_id=" + m_ConvID + ";");
        if(!MessageQuery.exec())
        {
            QMessageBox::information(this, "ERROR", MessageQuery.lastError().text());
        }
        MessageQuery.next();
        Destination = MessageQuery.value("Destination").toString();
        AddressBookQuery.prepare("SELECT * FROM Contacts WHERE c_id=" + Destination);
        if(!AddressBookQuery.exec())
        {
            QMessageBox::information(this, "ERROR", AddressBookQuery.lastError().text());
        }
        AddressBookQuery.next();
        Destination = AddressBookQuery.value("phonenumber").toString();
        Destination = "+1" + Destination;
        m_Radio->SendTextMessage(Destination, SendingBox->text());
        emit MessageSent();
        /*Adding Sent Message*/
        AddressBookQuery.prepare("SELECT name FROM Contacts WHERE c_id=0");
        AddressBookQuery.exec();
        AddressBookQuery.next();
        AddMessage(AddressBookQuery.value("name").toString(), SendingBox->text());
        SendingBox->clear();
    }
}

void TextMessageWindow::AddMessage(QString Source, QString Message)
{
    m_MessageList->append(Source + ": " + Message + "\r\n");
}

void TextMessageWindow::BasicInit()
{
    //DataBaseInit();
    m_MessageList = new QTextEdit;
    m_MessageList->setFocusPolicy(Qt::NoFocus);
    m_MessageList->setReadOnly(true);
    m_NewMessage = new QLineEdit;
    m_NewMessage->setFocusPolicy(Qt::StrongFocus);
    connect(m_NewMessage, &QLineEdit::returnPressed, this, &TextMessageWindow::ReturnPressed);
    m_Layout = new QVBoxLayout;
    m_Recipient = new QLabel("Someone Something");
    m_Recipient->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
    m_Layout->addWidget(m_Recipient);
    m_Layout->addWidget(m_MessageList);
    m_Layout->addWidget(m_NewMessage);

    setLayout(m_Layout);
}
