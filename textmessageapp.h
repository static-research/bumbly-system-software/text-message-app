#ifndef TEXTMESSAGEAPP_H
#define TEXTMESSAGEAPP_H

#include <QWidget>
#include <QVBoxLayout>
#include <QMenu>
#include <QToolButton>
#include <QScrollArea>
#include <QObject>
#include <QLabel>
#include <QTimer>

#include "textmessagewindow.h"
#include "common-library/networkinfo.h"
#include "common-library/TextMessage.hpp"
#include "common-library/addressbook.h"
#include "common-library/cellularradio.h"

class TextMessageApp : public QWidget
{
public:
    TextMessageApp();
    ~TextMessageApp();
private slots:
    void ReceiveTextMessage(TextMessage msg);
    void StartNewMessageThread(QString AcceptedContact);
    void StartAddressBook();
    void StartExistingMessageThread();
    void UpdateMessageList();
    void CheckForNewMessages();

private:
    QScrollArea * m_MessageScrollArea;
    QVBoxLayout * m_MessageLayout;
    //QMenu * m_MessageThreads;
    QToolButton * m_MessageThreads;
    QToolButton * m_CheckForMessages;
    CellularRadio * m_Radio;
    QVBoxLayout * m_GlobalLayout;
    NetworkInfo * m_NetworkInfo;
    QTimer * m_Timer;
};

#endif // TEXTMESSAGEAPP_H
